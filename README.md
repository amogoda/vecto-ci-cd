# CI scripts for VECTO project

For a general overview of the CI/CD and detailed diagrams refer to the [wiki home page](https://code.europa.eu/vecto/vectoci/-/wikis/home).

## Setup of CI/CD on VECTO projects

To setup personal forks and vecto projects follow the *Setup instructions* in the wiki.

Wiki [Setup section](https://code.europa.eu/vecto/vectoci/-/wikis/VECTO-CI/Setup).

## SAST scanning

Contains AutoDevOps CI pipeline-code `./legent-sast.yml` to scan sources for:

- [secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#edit-the-gitlab-ciyml-file-manually). See reference [`secrets` yml file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml).
- [Static analysis](https://docs.gitlab.com/ee/user/application_security/sast/index.html). See reference [`sast` analysis yml file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml#L164).

## Docker
The docker image use in this CI/CD project for vecto are define in dedicated branch `img-vecto-win` located [here](https://code.europa.eu/vecto/vectoci/-/tree/img-vecto-win?ref_type=heads).
